FROM alpine:latest

RUN apk update && apk upgrade && apk add bash bash-doc bash-completion 

RUN apk add --no-cache --virtual git && \
    apk add --update nodejs nodejs-npm && \
    npm i npm@latest -g && \
    npm install -g @angular/cli && \
    npm install -g typescript
